package com.hello.youtubeplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.hello.youtubeplayer.model.Video;

public class PlayVideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private static final String API_KEY = "AIzaSyD1QtPYzLpaErpj3vOGCQ4nVzL6gOTcVoU";
    private String VIDEO_ID;
    private String TITLE_VIDEO;
    YouTubePlayerView youTubeView;
    TextView txtName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        initView();
    }

    private void initView() {
        youTubeView = findViewById(R.id.youtube_view);
        txtName = findViewById(R.id.txtName);
        youTubeView.initialize(API_KEY, PlayVideoActivity.this);
        Bundle bundle = getIntent().getExtras();
        Video video = (Video) bundle.getSerializable("Video");
        TITLE_VIDEO = video.getTitle();
        txtName.setText(TITLE_VIDEO);
        VIDEO_ID = video.getUrlVideo();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.setShowFullscreenButton(true);
            youTubePlayer.cueVideo(VIDEO_ID);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "no internet", Toast.LENGTH_LONG).show();
    }
}
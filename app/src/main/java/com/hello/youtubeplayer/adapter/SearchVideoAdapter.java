package com.hello.youtubeplayer.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hello.youtubeplayer.PlayVideoActivity;
import com.hello.youtubeplayer.R;
import com.hello.youtubeplayer.models.Item;

import java.util.List;

public class SearchVideoAdapter extends RecyclerView.Adapter<SearchVideoAdapter.ViewHolder> {

    private List<Item> items;
    private Activity activity;

    public SearchVideoAdapter(List<Item> items, Activity activity) {
        this.items = items;
        this.activity = activity;
    }

    public List<Item> getItems() {
        return items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_video, parent, false);
        ViewHolder myViewHoder = new ViewHolder(view);
        return myViewHoder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Item item = items.get(position);
        holder.txtSearchTitle.setText(item.getSnippet().getTitle());
        holder.txrSearchChannelTitle.setText(item.getSnippet().getChannelTitle());


        Glide.with(activity).load(item.getSnippet().getThumbnails().getHigh().getUrl())
                .centerCrop()
                .into(holder.imgSearchVideo);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, PlayVideoActivity.class);
                intent.putExtra("Video", item);
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgSearchVideo;
        private TextView txtSearchTitle;
        private TextView txrSearchChannelTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgSearchVideo = itemView.findViewById(R.id.img_search_video);
            txtSearchTitle = itemView.findViewById(R.id.txt_search_title);
            txrSearchChannelTitle = itemView.findViewById(R.id.txt_search_channel_title);
        }
    }
}

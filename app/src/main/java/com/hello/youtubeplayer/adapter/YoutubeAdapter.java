package com.hello.youtubeplayer.adapter;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hello.youtubeplayer.PlayVideoActivity;
import com.hello.youtubeplayer.R;
import com.hello.youtubeplayer.model.Video;

import java.util.ArrayList;
import java.util.List;

public class YoutubeAdapter extends RecyclerView.Adapter<YoutubeAdapter.ViewHoder> {

    private Activity activity;
    private ArrayList<Video> videoList;

    public YoutubeAdapter(Activity activity, ArrayList<Video> videoList) {
        this.activity = activity;
        this.videoList = videoList;
    }

    @NonNull
    @Override
    public ViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false);
        ViewHoder myViewHoder = new ViewHoder(view);
        return myViewHoder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoder holder, int position) {
        final Video video = videoList.get(position);
        holder.txtTitle.setText(video.getTitle());
        holder.txtChannelTitle.setText(video.getChannelTitle());

        Glide.with(activity).load(video.getThumnail())
                .centerCrop()
                .into(holder.imgVideo);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("Video", video);
                Intent intent = new Intent(activity, PlayVideoActivity.class);
                intent.putExtras(bundle);
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public class ViewHoder extends RecyclerView.ViewHolder {

        private ImageView imgVideo;
        private TextView txtTitle;
        private TextView txtChannelTitle;

        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            imgVideo = itemView.findViewById(R.id.img_Video);
            txtTitle = itemView.findViewById(R.id.txt_title);
            txtChannelTitle = itemView.findViewById(R.id.txt_channelTitle);
        }
    }
}

package com.hello.youtubeplayer.model;

import java.io.Serializable;

public class Video implements Serializable {

    private String urlVideo;
    private String thumnail;
    private String title;
    private String channelTitle;

    public Video() {
    }

    public Video(String urlVideo, String thumnail, String title, String channelTitle) {
        this.urlVideo = urlVideo;
        this.thumnail = thumnail;
        this.title = title;
        this.channelTitle = channelTitle;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getThumnail() {
        return thumnail;
    }

    public void setThumnail(String thumnail) {
        this.thumnail = thumnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }
}
